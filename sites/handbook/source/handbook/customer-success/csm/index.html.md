---
layout: handbook-page-toc
title: "Customer Success Management Handbook"
description: "The Customer Success Management team at GitLab is a part of the Customer Success department, acting as trusted advisors to our customers and helping them realize value faster."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[<button class="btn btn-primary" type="button" style="padding: 15px 30px; font-size: 24px;">Digital Touch</button>](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Scale</button>](/handbook/customer-success/csm/segment/scale/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Growth</button>](/handbook/customer-success/csm/segment/mid-touch/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Strategic</button>](/handbook/customer-success/csm/segment/strategic/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">CSE</button>](/handbook/customer-success/csm/cse/)

## Mission Statement

Aligning passionate CSMs with customers to ensure their success by...  
- Driving progress aligned with their business outcomes
- Identifying and enabling the customer in their current and future GitLab use cases
- Expanding ROI with GitLab  

## What is a Customer Success Manager (CSM) at GitLab?

CSMs are accountable for customer adoption, measurable outcomes, customer satisfaction, and creating true customer advocacy. We create successful customers by enabling, training, and nurturing them throughout their journey. The following areas incorporate the remit of a CSM: 

- **Customer Adoption -**  Ensure the customer is working towards or adopting their desired use cases to maturity. Ensure the successful onboarding of all intended users. Identify areas of adoption risk and establish mitigating plays and programs. 
- **Delivering Positive Business Outcomes -** Ensure customers are meeting and exceeding their desired business outcomes so that customers can quantify and support their investment.
- **Trusted Advisor -** Establish "trusted advisor" relationships with the management and technical teams on the customer side while working seamlessly with our account team to deliver a best-in-class customer experience.
- **Account Expansion -** Lead adoption expansion beyond the customer's desired use cases and further customer return on investment (ROI). Partner with Sales to identify expansion opportunities and ensure we realize the expansion potential of a customer account.
- **Leading Business Reviews -**  Review and celebrate progress towards, or achievement of, the customer's desired business outcomes. Address challenges with a plan for mitigation, align on upcoming and future customer business objectives.

## Responsibilities and Services

Please reference this page for an overview of the areas your CSM will engage with you in: [CSM Points of Engagement](/handbook/customer-success/csm/services/)

## FY23 Goals (Big Rocks)
**Objective:**  Define the 3 ‘big rocks’ to take on in FY23 with the overall goal of moving our team forwards.  These rocks need to move us forwards as a team and as individuals, enabling us to scale, be impactful, and be inspired/fulfilled in our roles. 

**What are 'big rocks'?**  
As outlined in [this article](https://www.forbes.com/sites/hillennevins/2020/01/21/what-are-your-big-rocks/?sh=191f218fae34), they are our priorities, our mission-critical objectives that we need to solve for in the coming year. We arrived at this list through CSM leadership discussions and final input from individual contributors.

### Big Rock 1: (Re)Define the CSM Role
[Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/77) (GitLab Internal-Only)

The goal of the CSM is to drive growth. However, the role of the CSM has become increasingly broad over the past few years, deterring from the growth conversation with customers.  As we have added more to the role (migration & infrastructure guidance, cross-functional escalation ownership, consultative support), the ability to focus as a CSM on growth has become increasingly challenging. 

To continue to build a scalable CSM team focused on excellent customer experience and driving customer growth, we need to refine the role of the CSM.  This initiative will include what a CSM is and what a CSM is not.  

Key areas of exploration:
1. Definition by CSM segment: High-touch, Mid-touch, Scale
1. Reducing overlap: Looping in of support/timing
1. Infrastructure conversations and ownership
1. Escalation improvements; streamlining

### Big Rock 2: Expertise in driving CI and DevSecOps adoption & expansion
[Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/78) (GitLab Internal-Only)

In FY22 we put considerable focus into use case expansion.  The team have developed a well-rounded understanding of the use cases, but have not been given the space needed to become experts in driving any one use case.  In FY23 we will seek to become experts in driving CI and DevSecOps expansion and maturity, knowing these use cases are the key drivers for customer stickiness and growth. 

Key areas of exploration:
1. Iteration on playbooks
1. Supporting materials
1. Webinars
1. Digital enablement/expansion strategies
1. Reporting & visualization
1. Maturity scoring (customer)

### Big Rock 3: Deepen our customer engagement
[Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/79) (GitLab Internal-Only)

The CSM relationship with the customer has been architected primarily on engagement with GitLab admins, who are not responsible for platform adoption or business case value.  Due to the CSM being pigeon-holed into these admin conversations, we are often not engaging with the right people in order to ensure excellent customer experience, use case adoption and growth.  In FY23 we will seek to engage the development team leads and key users of the GitLab platform early and often, to ensure consistent growth strategies with our customers. 

Key areas of exploration: 
1. Expectation setting: from presales
1. Defining engagement strategies for target personas
1. Supporting content creation and digital programs
1. Exec sponsor program
1. User groups/customer forums


## Handbook Directory

[CSM Team Metrics Overview (VIDEO)](https://www.youtube.com/watch?v=9b8VviLG3yE&t=2s)

### CSM Learning & Development

- [Overview of available resources, training plans & career paths](/handbook/customer-success/csm/csm-development/)

### CSM Responsibilities

- [CSM Onboarding](/handbook/customer-success/csm/csm-onboarding/)
- [CSM Rhythm of Business](/handbook/customer-success/csm/rhythm/)
- [Using Gainsight](/handbook/customer-success/csm/gainsight/)
- [CSM Responsibilities and Services](/handbook/customer-success/csm/services/)
- [CSM and Product Interaction](/handbook/customer-success/csm/product/)
- [CSM and Professional Services Interaction](/handbook/customer-success/csm/engaging-with-ps/)
- [CSM and Support Interaction](/handbook/customer-success/csm/support/)
- [CSM and Partner Interaction](/handbook/customer-success/csm/engaging-with-partners/)
- [Escalation Process](/handbook/customer-success/csm/escalations/)
  *  [Infrastructure Escalation & Incident Process](/handbook/customer-success/csm/escalations/infrastructure/)
- [CSM-to-CSM Account Handoff](/handbook/customer-success/csm/account-handoff/)
- [CSM Roleplay Scenarios](/handbook/customer-success/csm/roleplays/)
- [CSM Retrospectives](/handbook/customer-success/csm/retrospectives/)
- [CSM PTO Guidelines](/handbook/customer-success/csm/pto/)
- [CSM READMEs](/handbook/customer-success/csm/readmes/) (Optional)

### Customer Journey

##### CSM-Assigned:

- [Transitioning a Customer from Pre-Sales to Post-Sales](/handbook/customer-success/pre-sales-post-sales-transition/)
- [Account Engagement and Prioritization](/handbook/customer-success/csm/engagement/)
   - [Non-Engaged Customer Strategies](/handbook/customer-success/csm/engagement/Non-engaged-customer-strategies/)
- [Account Onboarding](/handbook/customer-success/csm/onboarding/)
- [Success Plans](/handbook/customer-success/csm/success-plans/)
   - [Developer & Productivity Metrics](/handbook/customer-success/csm/metrics/)
   - [Sample Questions & Techniques for Getting to Good Customer Metrics](/handbook/customer-success/csm/success-plans/questions-techniques/)
- [Account Growth Plans](/handbook/customer-success/csm/account-growth/)
- [Stage Enablement & Stage Expansion - The Two Stage Adoption Motions](/handbook/customer-success/csm/stage-enablement-and-expansion/)
   - [Stage Adoption Metrics](/handbook/customer-success/csm/stage-adoption/)
   - [The Customer Value Received with Service Ping](/handbook/customer-success/csm/service-ping-faq/)
   - [Product Usage Data - Definitive Guide to Product Usage Data in Gainsight](/handbook/customer-success/product-usage-data/using-product-usage-data-in-gainsight/)
   - [Customer Use Case Adoption](/handbook/customer-success/product-usage-data/use-case-adoption/)
   - [Metrics Based Product Usage Playbooks](/handbook/customer-success/product-usage-data/metrics-based-playbooks/)
- [Cadence Calls](/handbook/customer-success/csm/cadence-calls/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/csm/ebr/)
- [Customer Renewal Tracking](/handbook/customer-success/csm/renewals/)
- [Customer Health Assessment and Risk Triage](/handbook/customer-success/csm/health-score-triage/)
- [Risk Types, Discovery & Mitigation](/handbook/customer-success/csm/risk-mitigation/)
- [Workshops and/or Lunch-and-Learns](/handbook/customer-success/csm/workshops/) 


##### Digital Customer Programs:

- [Digital Customer Programs Handbook Page](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/)
  * [CSM Assigned Customer Programs](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/#tam-assigned-account-programs)


### CSM Managers

- [CSM Manager Processes and CSM Leadership Team](/handbook/customer-success/csm/csm-manager/)
- [CSM Manager QBR Template](https://docs.google.com/presentation/d/1M18LeKTrzTIKNgl3Y_URC3z9xBUNUtPRJFiWXaEUuzM/edit?usp=sharing) (GitLab Internal)
- [CSM Promotion Template](https://docs.google.com/document/d/1UOcfUtrseaucIbnFmJkL8XsQwz4xKfj0IQcYbcRGSvI/edit) (GitLab Internal)


- - -


## CSM Tools

The following articulates where collaboration and customer management is owned:

1. [**Account Management Projects**](https://gitlab.com/gitlab-com/account-management): Shared project between GitLab team members and customer. Used to prioritize/plan work with customer.
   1. [**Ultimate Feature Exploration Checklist**](https://gitlab.com/gitlab-com/account-management/templates/customer-collaboration-project-template/-/blob/master/.gitlab/issue_templates/ultimate_features.md): Issue template that can be made into an issue in a customer's account management project. To be shared with the customer if they want to learn more about GitLab Ultimate features.
1. [**Google Drive**](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U): Internal. Used to capture call notes and other customer related documents.
1. [**Chorus**](/handbook/business-technology/tech-stack/#chorus): Internal. Used to record Zoom calls.
   1. [**Using Calendly with Chorus**](/handbook/customer-success/csm/calendly/): Instructions on Calendy set up with Chorus. 
1. [**Gainsight**](/handbook/customer-success/csm/gainsight/): Internal. Used to track customer health score, logging [customer activity](/handbook/customer-success/csm/gainsight/timeline/#activity-types) (i.e. calls, emails, meetings)
1. [**Issue Prioritization Dashboard**](/handbook/customer-success/csm/issue-prioritization/): Internal. Used to track customer requested issues.

### Education and Enablement

In Customer Success Management, it is important to be continuously learning more about our product and related industry topics. The [education and enablement handbook page](/handbook/customer-success/education-enablement) provides a dashboard of aggregated resources that we encourage you to use to get up to speed.

## SFDC useful reports 

### Tracking opportunities for your assigned Strategic Account Leader (SALs)

To ensure that opportunities are listed with the correct Order Type, [this Salesforce report](https://gitlab.my.salesforce.com/00O4M000004agfP) shows you all of the opportunities that have closed, or are soon to close, with your SALs. Tracking Order Type is important since CSM team quota and compensation depend on this. Please reference the latest [Sales Compensation Plan](/handbook/finance/sales-comp-plan/) information to know what is counted.

Next steps for you:

1. Customize [this SFDC report](https://gitlab.my.salesforce.com/00O4M000004agfP) where “Account Owner = your SALs”; “CSM = You”
1. Save report
1. Subscribe to report when “Record Count Greater Than 0” and Frequency = Weekly (You’ll get a weekly email as a reminder to look at the report)
1. If you find an opp that is tagged incorrectly, chatter (@Sales-Support) in the opportunity and let them know there is a mistake ([example](/handbook/customer-success/csm/#tam-tools))

## Related pages

- [Dogfooding in Customer Success](/handbook/customer-success/#dogfooding)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions)
- [Commercial Sales Customer Success](/handbook/customer-success/comm-sales/)
- [Customer Success' FAQ](/handbook/customer-success/faq/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Product Stages and the POCs for each](/handbook/product/categories/#devops-stages)
- [How to Provide Feedback to Product](/handbook/product/how-to-engage/#feedback-template)
- [Sales handbook](/handbook/sales/)
- [Support handbook](/handbook/support/)
- [Workshops and Lunch-and-Learn slides](https://drive.google.com/drive/folders/1qAymFTiXFEk-lRSNreIhaZ6Z62fdo_y2)
